# GTA V - Vehicle Controller #

This plugin will cover most wanted features in vehicles which R* always 'forget' to implement, even if it is requested since 90's.

## Features ##
* Full control of vehicle turn signals, including hazard alert flashing.
* Full control of vehicle hood and trunk, including vehicles such as cargobob, cargoplane, titan, etc.
* Full control of vehicle interior lights, where applicable.
* Switch some extra vehicle features, such as taxi light or police helicopter spotlight (not controllable yet).
* Leave the car while leave the engine running.
* Leave the car while leave the door open.
* Cruise control mode (This will keep your speed).
* Display on-screen speedometer (mph or Km/h).
* Fully configurable (Easy and well explained INI file).

## Requirements and Installation ##
* Download the file and extract both files into your GTAV directory.
* Open INI file and configure your keyboard binding (optional).
* Enjoy.

## Default Keys ##
* J = Left Indicator
* K = Alert Lights
* L = Right Indicator
* T = Toggle Cruise Control
* I = Extras (Taxi lights, or helicopter spotlight [not controllable yet]
* O = Toggle Engine
* CTRL + J = Open Hood
* CTRL + K = Interior Lights
* CTRL + L = Open Trunk
* G = Leave car with the engines running
* CTRL + G = Leave car with the door open and engines running.