/*
* Copyright 2015 Alexandre Leites. All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#pragma once

#define IS_CARGOBOB(m) ((m) == VEHICLE_CARGOBOB || (m) == VEHICLE_CARGOBOB2 || (m) == VEHICLE_CARGOBOB3)

enum VEHICLES : unsigned int
{
	VEHICLE_ADDER          = 3078201489, 	// 0xB779A091
	VEHICLE_AIRBUS         = 1283517198, 	// 0x4C80EB0E
	VEHICLE_AIRTUG         = 1560980623, 	// 0x5D0AAC8F
	VEHICLE_AKUMA          = 1672195559, 	// 0x63ABADE7
	VEHICLE_AMBULANCE      = 1171614426, 	// 0x45D56ADA
	VEHICLE_ANNIHILATOR    = 837858166, 	// 0x31F0B376
	VEHICLE_ARMYTANKER     = 3087536137, 	// 0xB8081009
	VEHICLE_ARMYTRAILER    = 2818520053, 	// 0xA7FF33F5
	VEHICLE_ARMYTRAILER2   = 2657817814, 	// 0x9E6B14D6
	VEHICLE_ASEA           = 2485144969, 	// 0x94204D89
	VEHICLE_ASEA2          = 2487343317, 	// 0x9441D8D5
	VEHICLE_ASTEROPE       = 2391954683, 	// 0x8E9254FB
	VEHICLE_BAGGER         = 2154536131, 	// 0x806B9CC3
	VEHICLE_BALETRAILER    = 3895125590, 	// 0xE82AE656
	VEHICLE_BALLER         = 3486135912, 	// 0xCFCA3668
	VEHICLE_BALLER2        = 142944341, 	// 0x08852855
	VEHICLE_BANSHEE        = 3253274834, 	// 0xC1E908D2
	VEHICLE_BARRACKS       = 3471458123, 	// 0xCEEA3F4B
	VEHICLE_BARRACKS2      = 1074326203, 	// 0x4008EABB
	VEHICLE_BATI           = 4180675781, 	// 0xF9300CC5
	VEHICLE_BATI2          = 3403504941, 	// 0xCADD5D2D
	VEHICLE_BENSON         = 2053223216, 	// 0x7A61B330
	VEHICLE_BFINJECTION    = 1126868326, 	// 0x432AA566
	VEHICLE_BIFF           = 850991848, 	// 0x32B91AE8
	VEHICLE_BISON          = 4278019151, 	// 0xFEFD644F
	VEHICLE_BISON2         = 2072156101, 	// 0x7B8297C5
	VEHICLE_BISON3         = 1739845664, 	// 0x67B3F020
	VEHICLE_BJXL           = 850565707, 	// 0x32B29A4B
	VEHICLE_BLAZER         = 2166734073, 	// 0x8125BCF9
	VEHICLE_BLAZER2        = 4246935337, 	// 0xFD231729
	VEHICLE_BLAZER3        = 3025077634, 	// 0xB44F0582
	VEHICLE_BLIMP          = 4143991942, 	// 0xF7004C86
	VEHICLE_BLISTA         = 3950024287, 	// 0xEB70965F
	VEHICLE_BMX            = 1131912276, 	// 0x43779C54
	VEHICLE_BOATTRAILER    = 524108981, 	// 0x1F3D44B5
	VEHICLE_BOBCATXL       = 1069929536, 	// 0x3FC5D440
	VEHICLE_BODHI2         = 2859047862, 	// 0xAA699BB6
	VEHICLE_BOXVILLE       = 2307837162, 	// 0x898ECCEA
	VEHICLE_BOXVILLE2      = 4061868990, 	// 0xF21B33BE
	VEHICLE_BOXVILLE3      = 121658888, 	// 0x07405E08
	VEHICLE_BUCCANEER      = 3612755468, 	// 0xD756460C
	VEHICLE_BUFFALO        = 3990165190, 	// 0xEDD516C6
	VEHICLE_BUFFALO2       = 736902334, 	// 0x2BEC3CBE
	VEHICLE_BULLDOZER      = 1886712733, 	// 0x7074F39D
	VEHICLE_BULLET         = 2598821281, 	// 0x9AE6DDA1
	VEHICLE_BURRITO        = 2948279460, 	// 0xAFBB2CA4
	VEHICLE_BURRITO2       = 3387490166, 	// 0xC9E8FF76
	VEHICLE_BURRITO3       = 2551651283, 	// 0x98171BD3
	VEHICLE_BURRITO4       = 893081117, 	// 0x353B561D
	VEHICLE_BURRITO5       = 1132262048, 	// 0x437CF2A0
	VEHICLE_BUS            = 3581397346, 	// 0xD577C962
	VEHICLE_BUZZARD        = 788747387, 	// 0x2F03547B
	VEHICLE_BUZZARD2       = 745926877, 	// 0x2C75F0DD
	VEHICLE_CABLECAR       = 3334677549, 	// 0xC6C3242D
	VEHICLE_CADDY          = 1147287684, 	// 0x44623884
	VEHICLE_CADDY2         = 3757070668, 	// 0xDFF0594C
	VEHICLE_CAMPER         = 1876516712, 	// 0x6FD95F68
	VEHICLE_CARBONIZZARE   = 2072687711, 	// 0x7B8AB45F
	VEHICLE_CARBONRS       = 11251904, 		// 0x00ABB0C0
	VEHICLE_CARGOBOB       = 4244420235, 	// 0xFCFCB68B
	VEHICLE_CARGOBOB2      = 1621617168, 	// 0x60A7EA10
	VEHICLE_CARGOBOB3      = 1394036463, 	// 0x53174EEF
	VEHICLE_CARGOPLANE     = 368211810, 	// 0x15F27762
	VEHICLE_CAVALCADE      = 2006918058, 	// 0x779F23AA
	VEHICLE_CAVALCADE2     = 3505073125, 	// 0xD0EB2BE5
	VEHICLE_CHEETAH        = 2983812512, 	// 0xB1D95DA0
	VEHICLE_COACH          = 2222034228, 	// 0x84718D34
	VEHICLE_COGCABRIO      = 330661258, 	// 0x13B57D8A
	VEHICLE_COMET2         = 3249425686, 	// 0xC1AE4D16
	VEHICLE_COQUETTE       = 108773431, 	// 0x067BC037
	VEHICLE_CRUISER        = 448402357, 	// 0x1ABA13B5
	VEHICLE_CRUSADER       = 321739290, 	// 0x132D5A1A
	VEHICLE_CUBAN800       = 3650256867, 	// 0xD9927FE3
	VEHICLE_CUTTER         = 3288047904, 	// 0xC3FBA120
	VEHICLE_DAEMON         = 2006142190, 	// 0x77934CEE
	VEHICLE_DILETTANTE     = 3164157193, 	// 0xBC993509
	VEHICLE_DILETTANTE2    = 1682114128, 	// 0x64430650
	VEHICLE_DINGHY         = 1033245328, 	// 0x3D961290
	VEHICLE_DINGHY2        = 276773164, 	// 0x107F392C
	VEHICLE_DLOADER        = 1770332643, 	// 0x698521E3
	VEHICLE_DOCKTRAILER    = 2154757102, 	// 0x806EFBEE
	VEHICLE_DOCKTUG        = 3410276810, 	// 0xCB44B1CA
	VEHICLE_DOMINATOR      = 80636076, 		// 0x04CE68AC
	VEHICLE_DOUBLE         = 2623969160, 	// 0x9C669788
	VEHICLE_DUBSTA         = 1177543287, 	// 0x462FE277
	VEHICLE_DUBSTA2        = 3900892662, 	// 0xE882E5F6
	VEHICLE_DUMP           = 2164484578, 	// 0x810369E2
	VEHICLE_DUNE           = 2633113103, 	// 0x9CF21E0F
	VEHICLE_DUNE2          = 534258863, 	// 0x1FD824AF
	VEHICLE_DUSTER         = 970356638, 	// 0x39D6779E
	VEHICLE_ELEGY2         = 3728579874, 	// 0xDE3D9D22
	VEHICLE_EMPEROR        = 3609690755, 	// 0xD7278283
	VEHICLE_EMPEROR2       = 2411965148, 	// 0x8FC3AADC
	VEHICLE_EMPEROR3       = 3053254478, 	// 0xB5FCF74E
	VEHICLE_ENTITYXF       = 3003014393, 	// 0xB2FE5CF9
	VEHICLE_EXEMPLAR       = 4289813342, 	// 0xFFB15B5E
	VEHICLE_F620           = 3703357000, 	// 0xDCBCBE48
	VEHICLE_FAGGIO2        = 55628203, 		// 0x0350D1AB
	VEHICLE_FBI            = 1127131465, 	// 0x432EA949
	VEHICLE_FBI2           = 2647026068, 	// 0x9DC66994
	VEHICLE_FELON          = 3903372712, 	// 0xE8A8BDA8
	VEHICLE_FELON2         = 4205676014, 	// 0xFAAD85EE
	VEHICLE_FELTZER2       = 2299640309, 	// 0x8911B9F5
	VEHICLE_FIRETRUK       = 1938952078, 	// 0x73920F8E
	VEHICLE_FIXTER         = 3458454463, 	// 0xCE23D3BF
	VEHICLE_FLATBED        = 1353720154, 	// 0x50B0215A
	VEHICLE_FORKLIFT       = 1491375716, 	// 0x58E49664
	VEHICLE_FQ2            = 3157435195, 	// 0xBC32A33B
	VEHICLE_FREIGHT        = 1030400667, 	// 0x3D6AAA9B
	VEHICLE_FREIGHTCAR     = 184361638, 	// 0x0AFD22A6
	VEHICLE_FREIGHTCONT1   = 920453016, 	// 0x36DCFF98
	VEHICLE_FREIGHTCONT2   = 240201337, 	// 0x0E512E79
	VEHICLE_FREIGHTGRAIN   = 642617954, 	// 0x264D9262
	VEHICLE_FREIGHTTRAILER = 3517691494, 	// 0xD1ABB666
	VEHICLE_FROGGER        = 744705981, 	// 0x2C634FBD
	VEHICLE_FROGGER2       = 1949211328, 	// 0x742E9AC0
	VEHICLE_FUGITIVE       = 1909141499, 	// 0x71CB2FFB
	VEHICLE_FUSILADE       = 499169875, 	// 0x1DC0BA53
	VEHICLE_FUTO           = 2016857647, 	// 0x7836CE2F
	VEHICLE_GAUNTLET       = 2494797253, 	// 0x94B395C5
	VEHICLE_GBURRITO       = 2549763894, 	// 0x97FA4F36
	VEHICLE_GRAINTRAILER   = 1019737494, 	// 0x3CC7F596
	VEHICLE_GRANGER        = 2519238556, 	// 0x9628879C
	VEHICLE_GRESLEY        = 2751205197, 	// 0xA3FC0F4D
	VEHICLE_HABANERO       = 884422927, 	// 0x34B7390F
	VEHICLE_HANDLER        = 444583674, 	// 0x1A7FCEFA
	VEHICLE_HAULER         = 1518533038, 	// 0x5A82F9AE
	VEHICLE_HEXER          = 301427732, 	// 0x11F76C14
	VEHICLE_HOTKNIFE       = 37348240, 		// 0x0239E390
	VEHICLE_INFERNUS       = 418536135, 	// 0x18F25AC7
	VEHICLE_INGOT          = 3005245074, 	// 0xB3206692
	VEHICLE_INTRUDER       = 886934177, 	// 0x34DD8AA1
	VEHICLE_ISSI2          = 3117103977, 	// 0xB9CB3B69
	VEHICLE_JACKAL         = 3670438162, 	// 0xDAC67112
	VEHICLE_JB700          = 1051415893, 	// 0x3EAB5555
	VEHICLE_JET            = 1058115860, 	// 0x3F119114
	VEHICLE_JETMAX         = 861409633, 	// 0x33581161
	VEHICLE_JOURNEY        = 4174679674, 	// 0xF8D48E7A
	VEHICLE_KHAMELION      = 544021352, 	// 0x206D1B68
	VEHICLE_LANDSTALKER    = 1269098716, 	// 0x4BA4E8DC
	VEHICLE_LAZER          = 3013282534, 	// 0xB39B0AE6
	VEHICLE_LGUARD         = 469291905, 	// 0x1BF8D381
	VEHICLE_LUXOR          = 621481054, 	// 0x250B0C5E
	VEHICLE_MAMMATUS       = 2548391185, 	// 0x97E55D11
	VEHICLE_MANANA         = 2170765704, 	// 0x81634188
	VEHICLE_MARQUIS        = 3251507587, 	// 0xC1CE1183
	VEHICLE_MAVERICK       = 2634305738, 	// 0x9D0450CA
	VEHICLE_MESA           = 914654722, 	// 0x36848602
	VEHICLE_MESA2          = 3546958660, 	// 0xD36A4B44
	VEHICLE_MESA3          = 2230595153, 	// 0x84F42E51
	VEHICLE_METROTRAIN     = 868868440, 	// 0x33C9E158
	VEHICLE_MINIVAN        = 3984502180, 	// 0xED7EADA4
	VEHICLE_MIXER          = 3510150843, 	// 0xD138A6BB
	VEHICLE_MIXER2         = 475220373, 	// 0x1C534995
	VEHICLE_MONROE         = 3861591579, 	// 0xE62B361B
	VEHICLE_MOWER          = 1783355638, 	// 0x6A4BD8F6
	VEHICLE_MULE           = 904750859, 	// 0x35ED670B
	VEHICLE_MULE2          = 3244501995, 	// 0xC1632BEB
	VEHICLE_NEMESIS        = 3660088182, 	// 0xDA288376
	VEHICLE_NINEF          = 1032823388, 	// 0x3D8FA25C
	VEHICLE_NINEF2         = 2833484545, 	// 0xA8E38B01
	VEHICLE_ORACLE         = 1348744438, 	// 0x506434F6
	VEHICLE_ORACLE2        = 3783366066, 	// 0xE18195B2
	VEHICLE_PACKER         = 569305213, 	// 0x21EEE87D
	VEHICLE_PATRIOT        = 3486509883, 	// 0xCFCFEB3B
	VEHICLE_PBUS           = 2287941233, 	// 0x885F3671
	VEHICLE_PCJ            = 3385765638, 	// 0xC9CEAF06
	VEHICLE_PENUMBRA       = 3917501776, 	// 0xE9805550
	VEHICLE_PEYOTE         = 1830407356, 	// 0x6D19CCBC
	VEHICLE_PHANTOM        = 2157618379, 	// 0x809AA4CB
	VEHICLE_PHOENIX        = 2199527893, 	// 0x831A21D5
	VEHICLE_PICADOR        = 1507916787, 	// 0x59E0FBF3
	VEHICLE_POLICE         = 2046537925, 	// 0x79FBB0C5
	VEHICLE_POLICE2        = 2667966721, 	// 0x9F05F101
	VEHICLE_POLICE3        = 1912215274, 	// 0x71FA16EA
	VEHICLE_POLICE4        = 2321795001, 	// 0x8A63C7B9
	VEHICLE_POLICEB        = 4260343491, 	// 0xFDEFAEC3
	VEHICLE_POLICEOLD1     = 2758042359, 	// 0xA46462F7
	VEHICLE_POLICEOLD2     = 2515846680, 	// 0x95F4C618
	VEHICLE_POLICET        = 456714581, 	// 0x1B38E955
	VEHICLE_POLMAV         = 353883353, 	// 0x1517D4D9
	VEHICLE_PONY           = 4175309224, 	// 0xF8DE29A8
	VEHICLE_PONY2          = 943752001, 	// 0x38408341
	VEHICLE_POUNDER        = 2112052861, 	// 0x7DE35E7D
	VEHICLE_PRAIRIE        = 2844316578, 	// 0xA988D3A2
	VEHICLE_PRANGER        = 741586030, 	// 0x2C33B46E
	VEHICLE_PREDATOR       = 3806844075, 	// 0xE2E7D4AB
	VEHICLE_PREMIER        = 2411098011, 	// 0x8FB66F9B
	VEHICLE_PRIMO          = 3144368207, 	// 0xBB6B404F
	VEHICLE_PROPTRAILER    = 356391690, 	// 0x153E1B0A
	VEHICLE_RADI           = 2643899483, 	// 0x9D96B45B
	VEHICLE_RAKETRAILER    = 390902130, 	// 0x174CB172
	VEHICLE_RANCHERXL      = 1645267888, 	// 0x6210CBB0
	VEHICLE_RANCHERXL2     = 1933662059, 	// 0x7341576B
	VEHICLE_RAPIDGT        = 2360515092, 	// 0x8CB29A14
	VEHICLE_RAPIDGT2       = 1737773231, 	// 0x679450AF
	VEHICLE_RATLOADER      = 3627815886, 	// 0xD83C13CE
	VEHICLE_REBEL          = 3087195462, 	// 0xB802DD46
	VEHICLE_REBEL2         = 2249373259, 	// 0x8612B64B
	VEHICLE_REGINA         = 4280472072, 	// 0xFF22D208
	VEHICLE_RENTALBUS      = 3196165219, 	// 0xBE819C63
	VEHICLE_RHINO          = 782665360, 	// 0x2EA68690
	VEHICLE_RIOT           = 3089277354, 	// 0xB822A1AA
	VEHICLE_RIPLEY         = 3448987385, 	// 0xCD935EF9
	VEHICLE_ROCOTO         = 2136773105, 	// 0x7F5C91F1
	VEHICLE_ROMERO         = 627094268, 	// 0x2560B2FC
	VEHICLE_RUBBLE         = 2589662668, 	// 0x9A5B1DCC
	VEHICLE_RUFFIAN        = 3401388520, 	// 0xCABD11E8
	VEHICLE_RUINER         = 4067225593, 	// 0xF26CEFF9
	VEHICLE_RUMPO          = 1162065741, 	// 0x4543B74D
	VEHICLE_RUMPO2         = 2518351607, 	// 0x961AFEF7
	VEHICLE_SABREGT        = 2609945748, 	// 0x9B909C94
	VEHICLE_SADLER         = 3695398481, 	// 0xDC434E51
	VEHICLE_SADLER2        = 734217681, 	// 0x2BC345D1
	VEHICLE_SANCHEZ        = 788045382, 	// 0x2EF89E46
	VEHICLE_SANCHEZ2       = 2841686334, 	// 0xA960B13E
	VEHICLE_SANDKING       = 3105951696, 	// 0xB9210FD0
	VEHICLE_SANDKING2      = 989381445, 	// 0x3AF8C345
	VEHICLE_SCHAFTER2      = 3039514899, 	// 0xB52B5113
	VEHICLE_SCHWARZER      = 3548084598, 	// 0xD37B7976
	VEHICLE_SCORCHER       = 4108429845, 	// 0xF4E1AA15
	VEHICLE_SCRAP          = 2594165727, 	// 0x9A9FD3DF
	VEHICLE_SEASHARK       = 3264692260, 	// 0xC2974024
	VEHICLE_SEASHARK2      = 3678636260, 	// 0xDB4388E4
	VEHICLE_SEMINOLE       = 1221512915, 	// 0x48CECED3
	VEHICLE_SENTINEL       = 1349725314, 	// 0x50732C82
	VEHICLE_SENTINEL2      = 873639469, 	// 0x3412AE2D
	VEHICLE_SERRANO        = 1337041428, 	// 0x4FB1A214
	VEHICLE_SHAMAL         = 3080461301, 	// 0xB79C1BF5
	VEHICLE_SHERIFF        = 2611638396, 	// 0x9BAA707C
	VEHICLE_SHERIFF2       = 1922257928, 	// 0x72935408
	VEHICLE_SKYLIFT        = 1044954915, 	// 0x3E48BF23
	VEHICLE_SPEEDO         = 3484649228, 	// 0xCFB3870C
	VEHICLE_SPEEDO2        = 728614474, 	// 0x2B6DC64A
	VEHICLE_SQUALO         = 400514754, 	// 0x17DF5EC2
	VEHICLE_STANIER        = 2817386317, 	// 0xA7EDE74D
	VEHICLE_STINGER        = 1545842587, 	// 0x5C23AF9B
	VEHICLE_STINGERGT      = 2196019706, 	// 0x82E499FA
	VEHICLE_STOCKADE       = 1747439474, 	// 0x6827CF72
	VEHICLE_STOCKADE3      = 4080511798, 	// 0xF337AB36
	VEHICLE_STRATUM        = 1723137093, 	// 0x66B4FC45
	VEHICLE_STRETCH        = 2333339779, 	// 0x8B13F083
	VEHICLE_STUNT          = 2172210288, 	// 0x81794C70
	VEHICLE_SUBMERSIBLE    = 771711535, 	// 0x2DFF622F
	VEHICLE_SULTAN         = 970598228, 	// 0x39DA2754
	VEHICLE_SUNTRAP        = 4012021193, 	// 0xEF2295C9
	VEHICLE_SUPERD         = 1123216662, 	// 0x42F2ED16
	VEHICLE_SURANO         = 384071873, 	// 0x16E478C1
	VEHICLE_SURFER         = 699456151, 	// 0x29B0DA97
	VEHICLE_SURFER2        = 2983726598, 	// 0xB1D80E06
	VEHICLE_SURGE          = 2400073108, 	// 0x8F0E3594
	VEHICLE_TACO           = 1951180813, 	// 0x744CA80D
	VEHICLE_TAILGATER      = 3286105550, 	// 0xC3DDFDCE
	VEHICLE_TANKER         = 3564062519, 	// 0xD46F4737
	VEHICLE_TANKERCAR      = 586013744, 	// 0x22EDDC30
	VEHICLE_TAXI           = 3338918751, 	// 0xC703DB5F
	VEHICLE_TIPTRUCK       = 48339065, 		// 0x02E19879
	VEHICLE_TIPTRUCK2      = 3347205726, 	// 0xC7824E5E
	VEHICLE_TITAN          = 1981688531, 	// 0x761E2AD3
	VEHICLE_TORNADO        = 464687292, 	// 0x1BB290BC
	VEHICLE_TORNADO2       = 1531094468, 	// 0x5B42A5C4
	VEHICLE_TORNADO3       = 1762279763, 	// 0x690A4153
	VEHICLE_TORNADO4       = 2261744861, 	// 0x86CF7CDD
	VEHICLE_TOURBUS        = 1941029835, 	// 0x73B1C3CB
	VEHICLE_TOWTRUCK       = 2971866336, 	// 0xB12314E0
	VEHICLE_TOWTRUCK2      = 3852654278, 	// 0xE5A2D6C6
	VEHICLE_TR2            = 2078290630, 	// 0x7BE032C6
	VEHICLE_TR3            = 1784254509, 	// 0x6A59902D
	VEHICLE_TR4            = 2091594960, 	// 0x7CAB34D0
	VEHICLE_TRACTOR        = 1641462412, 	// 0x61D6BA8C
	VEHICLE_TRACTOR2       = 2218488798, 	// 0x843B73DE
	VEHICLE_TRACTOR3       = 1445631933, 	// 0x562A97BD
	VEHICLE_TRAILERLOGS    = 2016027501, 	// 0x782A236D
	VEHICLE_TRAILERS       = 3417488910, 	// 0xCBB2BE0E
	VEHICLE_TRAILERS2      = 2715434129, 	// 0xA1DA3C91
	VEHICLE_TRAILERS3      = 2236089197, 	// 0x8548036D
	VEHICLE_TRAILERSMALL   = 712162987, 	// 0x2A72BEAB
	VEHICLE_TRASH          = 1917016601, 	// 0x72435A19
	VEHICLE_TRFLAT         = 2942498482, 	// 0xAF62F6B2
	VEHICLE_TRIBIKE        = 1127861609, 	// 0x4339CD69
	VEHICLE_TRIBIKE2       = 3061159916, 	// 0xB67597EC
	VEHICLE_TRIBIKE3       = 3894672200, 	// 0xE823FB48
	VEHICLE_TROPIC         = 290013743, 	// 0x1149422F
	VEHICLE_TVTRAILER      = 2524324030, 	// 0x967620BE
	VEHICLE_UTILLITRUCK    = 516990260, 	// 0x1ED0A534
	VEHICLE_UTILLITRUCK2   = 887537515, 	// 0x34E6BF6B
	VEHICLE_UTILLITRUCK3   = 2132890591, 	// 0x7F2153DF
	VEHICLE_VACCA          = 338562499, 	// 0x142E0DC3
	VEHICLE_VADER          = 4154065143, 	// 0xF79A00F7
	VEHICLE_VELUM          = 2621610858, 	// 0x9C429B6A
	VEHICLE_VIGERO         = 3469130167, 	// 0xCEC6B9B7
	VEHICLE_VOLTIC         = 2672523198, 	// 0x9F4B77BE
	VEHICLE_VOODOO2        = 523724515, 	// 0x1F3766E3
	VEHICLE_WASHINGTON     = 1777363799, 	// 0x69F06B57
	VEHICLE_YOUGA          = 65402552, 		// 0x03E5F6B8
	VEHICLE_ZION           = 3172678083, 	// 0xBD1B39C3
	VEHICLE_ZION2          = 3101863448, 	// 0xB8E2AE18
	VEHICLE_ZTYPE          = 758895617, 	// 0x2D3BD401
	VEHICLE_BIFTA          = 3945366167, 	// 0xEB298297
	VEHICLE_KALAHARI       = 92612664, 		// 0x05852838
	VEHICLE_PARADISE       = 1488164764, 	// 0x58B3979C
	VEHICLE_SPEEDER        = 231083307, 	// 0x0DC60D2B
	VEHICLE_BTYPE          = 117401876, 	// 0x06FF6914
	VEHICLE_JESTER         = 2997294755, 	// 0xB2A716A3
	VEHICLE_TURISMOR       = 408192225, 	// 0x185484E1
	VEHICLE_ALPHA          = 767087018, 	// 0x2DB8D1AA
	VEHICLE_VESTRA         = 1341619767, 	// 0x4FF77E37
	VEHICLE_MASSACRO       = 4152024626, 	// 0xF77ADE32
	VEHICLE_ZENTORNO       = 2891838741, 	// 0xAC5DF515
	VEHICLE_HUNTLEY        = 486987393, 	// 0x1D06D681
	VEHICLE_THRUST         = 1836027715, 	// 0x6D6F8F43
	VEHICLE_RHAPSODY       = 841808271, 	// 0x322CF98F
	VEHICLE_WARRENER       = 1373123368, 	// 0x51D83328
	VEHICLE_BLADE          = 3089165662, 	// 0xB820ED5E
	VEHICLE_GLENDALE       = 75131841, 		// 0x047A6BC1
	VEHICLE_PANTO          = 3863274624, 	// 0xE644E480
	VEHICLE_DUBSTA3        = 3057713523, 	// 0xB6410173
	VEHICLE_PIGALLE        = 1078682497, 	// 0x404B6381
	VEHICLE_MONSTER        = 3449006043, 	// 0xCD93A7DB
	VEHICLE_SOVEREIGN      = 743478836, 	// 0x2C509634
	VEHICLE_BESRA          = 1824333165, 	// 0x6CBD1D6D
	VEHICLE_MILJET         = 165154707, 	// 0x09D80F93
	VEHICLE_COQUETTE2      = 1011753235, 	// 0x3C4E2113
	VEHICLE_SWIFT          = 3955379698, 	// 0xEBC24DF2
	VEHICLE_INNOVAT        = 1153755511, 	// 0x44C4E977
	VEHICLE_HAKUCHOU       = 1265391242, 	// 0x4B6C568A
	VEHICLE_FUROREGT       = 3205927392, 	// 0xBF1691E0
	VEHICLE_JESTER2        = 3188613414, 	// 0xBE0E6126
	VEHICLE_MASSACRO2      = 3663206819, 	// 0xDA5819A3
	VEHICLE_RATLOADER2     = 3705788919, 	// 0xDCE1D9F7
	VEHICLE_SLAMVAN        = 729783779, 	// 0x2B7F9DE3
	VEHICLE_MULE3          = 2242229361, 	// 0x85A5B471
	VEHICLE_VELUM2         = 1077420264, 	// 0x403820E8
	VEHICLE_TANKER2        = 1956216962, 	// 0x74998082
	VEHICLE_CASCO          = 941800958, 	// 0x3822BDFE
	VEHICLE_BOXVILLE4      = 444171386, 	// 0x1A79847A
	VEHICLE_HYDRA          = 970385471, 	// 0x39D6E83F
	VEHICLE_INSURGENT      = 2434067162, 	// 0x9114EADA
	VEHICLE_INSURGENT2     = 2071877360, 	// 0x7B7E56F0
	VEHICLE_GBURRITO2      = 296357396, 	// 0x11AA0E14
	VEHICLE_TECHNICAL      = 2198148358, 	// 0x83051506
	VEHICLE_DINGHY3        = 509498602, 	// 0x1E5E54EA
	VEHICLE_SAVAGE         = 4212341271, 	// 0xFB133A17
	VEHICLE_ENDURO         = 1753414259, 	// 0x6882FA73
	VEHICLE_GUARDIAN       = 2186977100, 	// 0x825A9F4C
	VEHICLE_LECTRO         = 640818791, 	// 0x26321E67
	VEHICLE_KURUMA         = 2922118804, 	// 0xAE2BFE94
	VEHICLE_KURUMA2        = 410882957, 	// 0x187D938D
	VEHICLE_TRASH2         = 3039269212, 	// 0xB527915C
	VEHICLE_BARRACKS3      = 630371791, 	// 0x2592B5CF
	VEHICLE_VALKYRIE       = 2694714877, 	// 0xA09E15FD
	VEHICLE_SLAMVAN2       = 833469436, 	// 0x31ADBBFC
	VEHICLE_MARSHALL       = 1233534620, 	// 0x49863E9C
	VEHICLE_DUKES2         = 3968823444, 	// 0xEC8F7094
	VEHICLE_BLISTA3        = 3703315515, 	// 0xDCBC1C3B
	VEHICLE_BLISTA2        = 1039032026, 	// 0x3DEE5EDA
	VEHICLE_DODO           = 3393804037, 	// 0xCA495705
	VEHICLE_SUBMERSIBLE2   = 3228633070, 	// 0xC07107EE
	VEHICLE_BUFFALO3       = 237764926, 	// 0xE2C013E
	VEHICLE_DUKES          = 723973206, 	// 0x2B26F456
	VEHICLE_STALION        = 1923400478, 	// 0x72A4C31E
	VEHICLE_STALION2       = 3893323758, 	// 0xE80F67EE
	VEHICLE_DOMINATOR2     = 3379262425, 	// 0xC96B73D9
	VEHICLE_GAUNTLET2      = 349315417, 	// 0x14D22159
	VEHICLE_BLIMP2         = 3681241380, 	// 0xDB6B4924
};
